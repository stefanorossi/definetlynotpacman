# README #

#Requirement:
JDK 6 + NetBeans

#Version 1.2

-Main menu with background animation and music

-2 playable maps based on the true pacman version

-Bacgroun music and dead animation

-2 alternative modalities:

	- Auto pacman: Ramdomly spawned ghosts will try to cath PacMan and he will automatically move away from them.
	
	-Survival game: Ramdomly spawned ghosts will try to cath PacMan. You need to escape from them. You can place 
	and remove blocks from the map. Ghost will increase speed with time. The more you survive the more you will score.


#Installation and run

Clone the repo:
git clone https://stefanorossi@bitbucket.org/stefanorossi/definetlynotpacman.git


This is a NetBeans Project. Using NetBeans isnt mandatory but recommended for the built-in applet runner.
You can run the GrigliaApplet.java class with netbeans.


#Description
This project started with the study of the Manhattan alghoritm wich is the IA used both by ghosts 
trying to catch PacMan and by pacman in the Auto PacMan modality. 

You can move PacMan with wasd pattern.

![Scheme](images/notpacmanmenu.png)


This is the first map:

![Scheme](images/notpacmanl1.png)

#License 


All my work is released under [DBAD](https://www.dbad-license.org/) license.



