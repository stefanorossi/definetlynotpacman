/*
 * Questa classe serve impostare se una cella è un muro o è libera, e memorizza
 * un valore intero
 */
package pacman;

/**
 *
 * @author BBC
 */
public class Cella {
	boolean muro; 	//false = libera, true = muro
	int passi;      //memorizzo qui la distanza in passi da pacman
	
        ///////////////////////////////////////////
        //COSTRUTTORI
        ///////////////////////////////////////////
	public Cella(){
		this.muro = false;
		this.passi = 0;
	}
        
        ///////////////////////////////////////////
        //GET DI TUTTI GLI ATTRIBUTI
        ///////////////////////////////////////////
	public boolean isMuro(){
		if(this.muro == true) {
                return true;
            }
		else {
                return false;
            }
	}
        
	public int getPassi(){
		return this.passi;
	}
        
        ///////////////////////////////////////////
        //SET RESET DI TUTTI GLI ATTRIBUTI
        ///////////////////////////////////////////
	public void setStato(String stato){
		if(stato == "libera") {
                this.muro = false;
            }
		else {
                this.muro = true;
            }
	}
	public void setPassi(int passi){
		this.passi = passi;
	}
        public void resetPassi(){
		this.passi = 0;
	}
}
