/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pacman;

/**
 *
 * @author BBC
 */
public class Cibo {
    ///////////////////////////////////////////
    //ATTRIBUTI
    ///////////////////////////////////////////
    private int riga; //riga in è
    private int colonna; //colonna in cui è
    private short ttl;     //time to live, tempo dopo la quale si annullerà
    private short ttlrel;   //memorizzazione ttl relativo
    private int valore;  //valore del cibo
    private boolean mangiato; //true se il cibo è gia stato mangiato
    
    
    ///////////////////////////////////////////
    //COSTRUTTORI
    ///////////////////////////////////////////
    public Cibo(){
        this.riga = 0;
        this.colonna = 0;
        this.ttlrel = 0;
        this.valore = 200; //assegna una valore casuale da 1000 a 5000 a tep di 1000
        this.ttl = 0;
        this.mangiato = false;
    }
    public Cibo(int r, int c){
        this.riga = r;
        this.colonna = c;
        this.ttlrel = 0;
        this.valore = 200; //assegna una valore casuale da 1000 a 5000 a tep di 1000
        this.ttl = 0;
        this.mangiato = false;
    }    
    ///////////////////////////////////////////
    //GETTER
    ///////////////////////////////////////////
    public int getRiga(){
        return this.riga;
    }
    public int getColonna(){
        return this.colonna;
    }
    public short getTimeToLive(){
        return this.ttl;
    }
    public short getTimeToLiveRel(){
        return this.ttlrel;
    }
    public int getValore(){
        return this.valore;
    }
    public boolean isMangiato(){
        return this.mangiato;
    }
    
    ///////////////////////////////////////////
    //SETTER
    ///////////////////////////////////////////
    public void setRiga(int riga){
        this.riga = riga;
    }
    public void setColonna(int colonna){
        this.colonna = colonna;
    }
    public void setTimeToLive(short ttl){
        this.ttl = ttl;
    }
    public void setTimeToLiveRel(short ttlrel){
        this.ttlrel = ttlrel;
    }
    public void setValore(int valore){
        this.valore = valore;
    }
    public void mangiato(){
        this.mangiato = true;
    }
    public void nonMangiato(){
        this.mangiato = false;
    }
    ///////////////////////////////////////////
    //METODI
    ///////////////////////////////////////////
    public void setValoreRandom(){
        this.valore = randomValue();
    }
    public int randomValue(){
        //numero casuale da 1000 a 5000 a step di 1000 che è il punteggio
        return (int)(Math.round(Math.random() * 4) + 1) * 500;
    }
    public void setTtlRandom(){
        this.ttl = randomTtl();
    }
    public short randomTtl(){
        //numero casuale da 5 a 10 e sarebbero i secondi di vita
        return (short)(Math.round(Math.random() * 5) + 5);
    }
}
